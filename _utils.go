package telnet

import (
	"log"
	"os"
	"time"
)

const timeout = 1 * time.Second
const PROMPT = "#"

func CheckErr(err error) {
	if err != nil {
		log.Fatalln("Error:", err)
	}
}

func Connect(dst string) (*TelnetConn, error) {
	//
	//timeout := 1 * time.Second
	t, err := DialTimeout("tcp", dst, timeout)
	if err == nil {
		t.SetUnixWriteMode(true)
	}
	return t, err
}

func Expect(t *TelnetConn, d ...string) error {

	var err error

	err = t.SetReadDeadline(time.Now().Add(timeout))
	if err == nil {
		err = t.SkipUntil(d...)
	}

	return err
}

func Sendln(t *TelnetConn, s string) error {

	var err error
	err = t.SetWriteDeadline(time.Now().Add(timeout))
	if err == nil {
		buf := make([]byte, len(s)+1)
		copy(buf, s)
		buf[len(s)] = '\n'
		_, err = t.Write(buf)
		//err = err.Err
	}
	return err
}

func Login(t *TelnetConn, user, passwd string) error {
	/*
		login :  "

	*/
	var err error
	login_prompt := "login: "
	password_prompt := "ssword: "
	line_prompt := PROMPT

	err = Expect(t, login_prompt)
	if err == nil {
		err = Sendln(t, user)
		if err == nil {
			err = Expect(t, password_prompt)
			if err == nil {
				err = Sendln(t, passwd)
				if err == nil {
					err = Expect(t, line_prompt)
				}
			}
		}
	}
	return err
}

func main() {
	//if len(os.Args) != 5 {
	//log.Printf("Usage: %s {unix|cisco} HOST:PORT USER PASSWD", os.Args[0])
	//return
	//}
	dst, user, passwd := "192.168.1.1", "root", "sah"

	t, err := Connect(dst)
	CheckErr(err)
	//t.SetUnixWriteMode(true)

	err = Login(t, user, passwd)
	CheckErr(err)

	Sendln(t, "ls -l")
	response, err := t.ReadBytes('#')
	os.Stdout.Write(response)
	os.Stdout.WriteString("\n")

}
