package telnet

import (
	"context"
	"errors"
	"fmt"
	"log"
	"time"

	"github.com/nats-io/nats"
)

/*

	a telnet session

*/

// default values for prompts
const (
	PROMPT          = "#"
	LOGIN_PROMPT    = "login: "
	PASSWORD_PROMPT = "ssword: "
	LINE_PROMPT     = "#"
)

type SessionParams struct {
	Name     string        `json:"name"`
	Host     string        `json:"host"`
	Port     int           `json:"port"`
	Login    string        `json:"login"`
	Password string        `json:"password"`
	Prompt   string        `json:"prompt"`
	Timeout  time.Duration `json:"timeout"` // time to wait for tcp dialIn
	LogLevel int           `json:"log_level"`
}

func NewSessionParams(name, host string, port int, login, password, prompt string, timeout int, logLevel int) (*SessionParams, error) {

	if prompt == "" {
		prompt = LINE_PROMPT
	}
	w := &SessionParams{
		Name:     name,
		Host:     host,
		Port:     port,
		Login:    login,
		Prompt:   prompt,
		Timeout:  time.Duration(timeout) * time.Millisecond,
		LogLevel: logLevel,
	}
	return w, nil
}

// Session
type Session struct {
	*nats.Conn
	TelnetConn *TelnetConn

	Params *SessionParams
}

// NewSession : create a telnet session
func NewSession(cnx *nats.Conn, params *SessionParams) (w *Session, err error) {

	if params.Name == "" {
		params.Name = "noname"
	}
	w = &Session{
		Conn:   cnx,
		Params: params,
	}
	return w, err
}

// Connect to telnet server and set TelnetConnection
func (s *Session) Connect() (err error) {

	host := s.Params.Host
	port := s.Params.Port
	timeout := s.Params.Timeout
	//timeout = 1 * time.Second
	dst := fmt.Sprintf("%s:%d", host, port)

	t, err := DialTimeout("tcp", dst, timeout)
	if err != nil {
		return err
	}
	t.SetUnixWriteMode(true)
	s.TelnetConn = t
	return err
}

func (s *Session) Check() (err error) {
	if s.TelnetConn == nil {
		err = errors.New("telnet session: not initialized")
	}
	return err
}

// Close the telnet connection
func (s *Session) Close() (err error) {
	if s.TelnetConn != nil {
		err = s.TelnetConn.Close()
	}
	return err
}

//// SetReadDeadLine
//func (s *Session) SetReadDeadLine( timeout time.Duration) error {
//	return s.TelnetConn.SetReadDeadline(time.Now().Add(timeout))
//}
//// SetWriteDeadLine
//func (s *Session) SetWriteDeadLine( timeout time.Duration) error {
//	return s.TelnetConn.SetReadDeadline(time.Now().Add(timeout))
//}

// SendLn : send a command line to telnet server
func (s *Session) SendLn(line string) error {

	timeout := s.Params.Timeout

	var err error
	err = s.TelnetConn.SetWriteDeadline(time.Now().Add(timeout))
	if err == nil {
		buf := make([]byte, len(line)+1)
		copy(buf, line)
		buf[len(line)] = '\n'
		_, err = s.TelnetConn.Write(buf)
		//err = err.Err
	}
	return err
}

// Expect skip until a string
func (s *Session) Expect(d ...string) (err error) {

	t := s.TelnetConn
	timeout := s.Params.Timeout
	err = t.SetReadDeadline(time.Now().Add(timeout))
	if err == nil {
		err = t.SkipUntil(d...)
	}

	return err
}

// ReadUntil read until a string
func (s *Session) ReadUntil(d ...string) (buffer []byte, err error) {

	t := s.TelnetConn
	timeout := s.Params.Timeout
	err = t.SetReadDeadline(time.Now().Add(timeout))
	if err == nil {
		buffer, err = t.ReadUntil(d...)
	}
	return buffer, err
}

// Login : performs a simple login sequence
func (s *Session) Login(linePrompt, loginPrompt, passwordPrompt string) (err error) {
	/*
		login :  "

	*/
	return s.login(linePrompt, loginPrompt, passwordPrompt)
}

// Login : performs a simple login sequence
func (s *Session) login(linePrompt, loginPrompt, passwordPrompt string) (err error) {
	/*
		login :  "

	*/
	if linePrompt == "" {
		linePrompt = LINE_PROMPT
	}
	if loginPrompt == "" {
		loginPrompt = LOGIN_PROMPT
	}
	if passwordPrompt == "" {
		passwordPrompt = PASSWORD_PROMPT
	}

	err = s.Check()
	if err != nil {
		return err
	}

	user := s.Params.Login
	passwd := s.Params.Password

	err = s.Expect(loginPrompt)
	if err != nil {
		err = fmt.Errorf("telnet session: did not get login prompt: %s", loginPrompt)
		return err
	}
	_ = s.SendLn(user)
	err = s.Expect(passwordPrompt)
	if err != nil {
		err = fmt.Errorf("telnet session: did not get password prompt: %s", passwordPrompt)
		return err
	}
	_ = s.SendLn(passwd)
	err = s.Expect(linePrompt)
	if err != nil {
		err = fmt.Errorf("telnet session: did not get line prompt: %s", linePrompt)
		return err
	}
	return err
}

// Run the telnet session
func (s *Session) Run(ctx context.Context) {
	//
	log.Printf("telnet: session [%s] starts\n", s.Params.Name)
	for {
		// asked for shutdown ?
		select {
		case <-ctx.Done():
			log.Printf("telnet: session [%s] exits\n", s.Params.Name)
			return
		default:
			// continue
			time.Sleep(1 * time.Second)
		}
	}
}
