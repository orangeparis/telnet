package telnet

import (
	"context"
	"fmt"
	"sync"
	"time"

	"github.com/nats-io/nats"
)

/*

	a controller for telnet sessions

*/

//var controller *Ipv4Watchers

type SessionHandler struct {
	Cancel context.CancelFunc
	//Params  *SessionParams
	Session *Session // a pointer to the active session
}

// Sessions a Contrller fot ipv4 watcher
type Sessions struct {
	mux  sync.Mutex
	Cnx  *nats.Conn
	Name string // resource name eg : session
	//MemberList []string
	Members map[string]SessionHandler
}

// NewSessions create an ipv4 watcher controller
func NewSessions(nc *nats.Conn, resourceNname string) *Sessions {
	c := &Sessions{Cnx: nc, Name: resourceNname}
	c.Members = make(map[string]SessionHandler)
	return c
}

// New create an new Session
func (w *Sessions) New(name string, params *SessionParams) (err error) {

	w.mux.Lock()
	defer w.mux.Unlock()

	// create the session
	session, err := NewSession(w.Cnx, params)
	if err != nil {
		return err
	}

	// set the member
	ctx, cancel := context.WithCancel(context.Background())
	h := SessionHandler{Cancel: cancel, Session: session}
	//h := SessionHandler{Cancel: cancel, Params: params, Session: session}
	w.Members[params.Name] = h

	// start the session
	_ = session
	go session.Run(ctx)

	return err

}

// Clear a session
func (w *Sessions) Clear(name string) {

	w.mux.Lock()
	defer w.mux.Unlock()

	handler, ok := w.Members[name]
	if ok {
		// cancel the watcher
		handler.Cancel()
		time.Sleep(1 * time.Second)
		delete(w.Members, name)
	}

}

// ClearAll all sessions
func (w *Sessions) ClearAll() {

	w.mux.Lock()
	defer w.mux.Unlock()

	for name := range w.Members {
		handler, ok := w.Members[name]
		if ok {
			// cancel the watcher
			handler.Cancel()
			delete(w.Members, name)
		}
	}
	time.Sleep(1 * time.Second)

}

// MemberList : list of watcher rid
func (w *Sessions) MemberList() (list []string) {
	w.mux.Lock()
	defer w.mux.Unlock()
	for name := range w.Members {
		list = append(list, w.GetRid(name))
	}
	return list
}

// IsMember : check a session exists
func (w *Sessions) IsMember(name string) bool {
	w.mux.Lock()
	defer w.mux.Unlock()
	_, ok := w.Members[name]
	return ok
}

// Get : get the item representation
func (w *Sessions) Get(name string) *SessionParams {
	item := w.Members[name]
	return item.Session.Params
}

// GetRid : get the session rid from name
func (w *Sessions) GetRid(name string) string {
	return fmt.Sprintf("telnet.session.%s", name)
}

//
func (w *Sessions) RetrieveSession(name string) (s *Session, err error) {
	w.mux.Lock()
	defer w.mux.Unlock()
	m, ok := w.Members[name]
	if ok == false {
		err := fmt.Errorf("session [%s] not found", name)
		return s, err
	}
	s = m.Session
	err = s.Check()
	return s, err
}
