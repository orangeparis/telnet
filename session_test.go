package telnet_test

import (
	"testing"

	"bitbucket.org/orangeparis/telnet"
	"github.com/nats-io/nats"
	//"github.com/stretchr/testify/assert"
	//"fmt"
)

func TestIpv4Session(t *testing.T) {

	nc, err := nats.Connect("nats://demo.nats.io:4222")
	if err != nil {
		t.Fail()
		return
	}
	defer nc.Close()

	p, err := telnet.NewSessionParams("hello", "localhost", 23, "root", "sah", "#", 10, 100)

	s, _ := telnet.NewSession(nc, p)

	defer s.Close()

	err = s.Connect()

}
