package service

import (
	"bitbucket.org/orangeparis/telnet"
	"github.com/jirenius/go-res"
	"github.com/nats-io/nats"
)

//var natsServer = "nats://demo.nats.io:4222"

// a controller to lauchn ipv4 watchers
var sessionController *telnet.Sessions

// NewService starts the nats service
func NewService(natsURL string) *res.Service {

	// create a ipv4 watcher controller
	nc, err := nats.Connect(natsURL)
	if err != nil {
		panic(err)
	}
	defer nc.Close()
	// create a controller
	sessionController = telnet.NewSessions(nc, "session")

	// Create a new RES Service
	s := res.NewService("telnet")

	// add ipv4 resource
	SessionResource(s)

	// Start the service
	go s.ListenAndServe(natsURL)

	return s
}
