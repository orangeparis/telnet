package service_test

import (
	"testing"
	"time"

	"bitbucket.org/orangeparis/telnet/client"
	"bitbucket.org/orangeparis/telnet/service"
	//"github.com/stretchr/testify/assert"
	//"fmt"
)

func TestService(t *testing.T) {

	natsURL := "nats://demo.nats.io:4222"

	// start the service
	s := service.NewService(natsURL)
	time.Sleep(2 * time.Second)

	//
	// send messages
	//

	// create a client
	cl, err := client.NewClient(natsURL, 5)
	if err != nil {
		panic(err)
	}
	defer cl.Conn.Close()

	// starts a session
	msg, err := cl.Open("local", "localhost", 23, "root", "sah", "#", 10, 90)
	if err != nil {
		println(err.Error())
	} else {
		println(string(msg.Data))
	}

	// send line to session
	msg, err = cl.SendLn("local", "I send you this line")
	if err != nil {
		println(err.Error())
	} else {
		println(string(msg.Data))
	}

	// list active sessions
	msg, err = cl.List()
	if err != nil {
		println(err.Error())
	} else {
		println(string(msg.Data))
	}

	// get an active session
	msg, err = cl.GetSession("local")
	if err != nil {
		println(err.Error())
	} else {
		println(string(msg.Data))
	}

	// kill all sessions
	msg, err = cl.Clear()
	if err != nil {
		println(err.Error())
	} else {
		println(string(msg.Data))
	}

	// kill an ipv4 watcher
	msg, err = cl.Close("local")
	if err != nil {
		println(err.Error())
	} else {
		println(string(msg.Data))
	}

	time.Sleep(10 * time.Second)
	// shutdown the service
	s.Shutdown()

}
