package service

import (
	"fmt"
	"strings"
	"time"

	"bitbucket.org/orangeparis/telnet"
	"github.com/jirenius/go-res"
)

/*

	resource ipwatch.ipv4

*/

// NewIn message for ipwatch.ipv4.new
type NewIn struct {
	Name     string `json:"name"`
	Host     string `json:"host"`
	Port     int    `json:"port"`
	Login    string `json:"login"`
	Password string `json:"password"`
	Prompt   string `json:"prompt"`
	Timeout  int    `json:"timeout"` // time to wait for tcp dialIn
	LogLevel int    `json:"log_level"`
}

// NewOut response message for telnet.session.new
type NewOut struct {
	Rid string `json:"rid"` // rid , resource  identier eg telnet.session.123
}

// MsgReadLnIn message in for readln( timeout, prompt )
type MsgReadUntilIn struct {
	Timeout int64  `json:"timeout"` // timeout in second
	Prompt  string `json:"prompt"`  //
}

// MsgReadLnOut message in for readln( timeout, prompt )
type MsgReadUntilOut struct {
	Lines []string // line read
}

// SessionResource :  handle resource telnet.session
func SessionResource(s *res.Service) {

	// handle collection ipwatch.ipv4 resource
	s.Handle("session",
		res.Access(res.AccessGranted),

		//  get.ipwatch.ipv4  list all ipv4 watchers
		res.GetCollection(func(r res.CollectionRequest) {
			r.Collection(sessionController.MemberList())
		}),

		//  call.ipwatch.ipv4.start  create an ipv4 watcher  ipwatch.ipv4.$id
		res.Call("open", func(r res.CallRequest) {
			var p = &NewIn{}
			r.ParseParams(p)
			// create a telnet session
			params, err := telnet.NewSessionParams(p.Name, p.Host, p.Port, p.Login, p.Password, p.Prompt, p.Timeout, p.LogLevel)
			//err := sessionController.New(p.Name, p.Cidr, p.Port, p.Timeout, 90)
			if err != nil {
				rerr := &res.Error{Code: "500", Message: err.Error()}
				r.Error(rerr)
			} else {
				_ = sessionController.New(params.Name, params)
				msg := &NewOut{Rid: fmt.Sprintf("telnet.session.%s", p.Name)}
				//msg := fmt.Sprintf("{\"rid\":\"ipwatch.ipv4.%s\"", p.Name)
				r.OK(msg)
			}
		}),

		//  call.telnet.session.clear     delete all sessions
		res.Call("clear", func(r res.CallRequest) {
			sessionController.ClearAll()
			r.OK("")
		}),
	)

	// handle telnet;session.$id resources
	s.Handle("session.$id",
		res.Access(res.AccessGranted),

		// get.telnet.session.$id  get watcher representation
		res.GetModel(func(r res.ModelRequest) {
			item := sessionController.Get(r.PathParam("id"))
			if item == nil {
				r.NotFound()
			} else {
				r.Model(item)
			}
		}),

		// call.telnet.session.$id.close
		res.Call("close", func(r res.CallRequest) {
			name := r.PathParam("id")
			sessionController.Clear(name)
			// create an ipfv4 watcher
			r.OK("")
		}),

		// call.telnet.session.$id.sendln   {line: }
		res.Call("sendln", func(r res.CallRequest) {
			// retrieve session by name
			name := r.PathParam("id")
			session, err := sessionController.RetrieveSession(name)
			if err != nil {
				r.NotFound()
				return
			}
			// parse parameters
			p := make(map[string]interface{})
			r.ParseParams(p)
			line := p["line"].(string)
			// send the line to telnet session
			err = session.SendLn(line)
			if err != nil {
				rerr := &res.Error{Code: "500", Message: err.Error()}
				r.Error(rerr)
				return
			} else {
				r.OK("")
				return
			}
		}),

		// read until delimiter ( prompt )
		res.Call("ReadUntil", func(r res.CallRequest) {
			// retrieve session by name
			name := r.PathParam("id")
			session, err := sessionController.RetrieveSession(name)
			if err != nil {
				r.NotFound()
				return
			}
			// parse parameters
			p := make(map[string]interface{})
			r.ParseParams(p)
			timeout := p["timeout"].(int64)
			prompt := p["prompt"].(string)
			//
			if prompt == "" {
				prompt = session.Params.Prompt
			}
			session.Params.Timeout = time.Duration(timeout) * time.Second
			data, err := session.ReadUntil(prompt)
			if err != nil {
				rerr := &res.Error{Code: "500", Message: err.Error()}
				r.Error(rerr)
				return
			}
			buffer := string(data)
			lines := strings.Split(buffer, prompt)
			result := &MsgReadUntilOut{Lines: lines}
			r.OK(result)
			return

		}),
	)
}
