package telnet_test

import (
	"testing"

	"bitbucket.org/orangeparis/telnet"
	"github.com/nats-io/nats"

	//"github.com/stretchr/testify/assert"

	//"fmt"

	"time"
)

func TestIpv4WatcherController(t *testing.T) {

	nc, err := nats.Connect("nats://demo.nats.io:4222")
	if err != nil {
		t.Fail()
		return
	}
	defer nc.Close()

	// create a controller
	c := telnet.NewSessions(nc, "session")

	// set a config for a session
	p, err := telnet.NewSessionParams("hello", "localhost", 23, "root", "sah", "#", 10, 100)

	//cnf := ipwatch.NewIpv4WatchersParams("sample", "192.168.1.0/24", 80, 500, 90)

	l1 := c.MemberList()
	_ = l1

	//ctx, cancel := context.WithCancel(context.Background())
	c.New("sample", p)

	l2 := c.MemberList()
	_ = l2

	time.Sleep(5 * time.Second)

	c.ClearAll()
	//c.Clear("sample")

	// stop the watcher
	//c.Members["sample"].Cancel()
	// cancel()
	time.Sleep(1 * time.Second)

}
