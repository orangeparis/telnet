package main

import (
	"flag"
	"log"

	"bitbucket.org/orangeparis/telnet/service"
)

/*
	runs an ipwatch nats service

*/

var natsURL string

func main() {

	var nats string
	flag.StringVar(&nats, "natsURL", "nats://demo.nats.io:4222", "nats server url")

	flag.Parse()

	log.Printf("telnet service: starts with nats server at : %s\n", nats)

	// start the ipwatch service
	service.NewService(nats)

	// wait forever
	select {}

	//log.Printf("ipwatch service: exit")
}
