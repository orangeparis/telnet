package main

import (
	"bitbucket.org/orangeparis/telnet/tests"
	"context"
	"log"
	"time"
)

/*
	play with the fake telnet server ( /tests/faketelnetserver.go)


	build and launch this file

	* go run faketelenetserver.play

	in another terminal open a telnet session

	* telnet 127.0.0.1 2323

	simulate a login , password

	type exit to leave

	The player will exit automatically after 1 minute



*/

func main() {
	ctx, cancel := context.WithCancel(context.Background())

	server, err := tests.NewFakeTelnetServer("2323", "", "", "")
	if err != nil {
		log.Println(err.Error())
		return
	}
	go server.Run(ctx)
	//go server2(ctx)

	time.Sleep(60 * time.Second)

	println("send cancel to listener")
	//cancel()
	_ = cancel
	server.Close()

	println("exiting")

	time.Sleep(1 * time.Second)

}
