# TELNET #

a nats service "telnet" to handle telnet sessions



# telnet.session  

handle telnet sessions

## resources and methods

## call.telnet.session.open

in:

    host string  // eg localhost
    port int     // tcp port ( 23 )
	login string    //  
	password string  // 
    prompt string    //  eg "#"
    timeout int
	
	
	
out:

	rid string : resource id  eg telnet.session.1234
	
## get.telnet.session

get a list of telent sessions

out:
	[rid1,rid2]  eg ["telnet.session.1",telnet.session.2"]
	
	
	
## get.telnet.session.$id

get representation of a telnet session

	host string  // eg localhost
    port int     // tcp port ( 23 )
	login string    //  
	password string  // 
    prompt string    //  eg "#"
    timeout int


## call.telnet.session.$id.close

shutdown an ipwatcher



