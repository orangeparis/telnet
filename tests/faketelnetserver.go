package tests

import (
	"bufio"
	"context"
	"fmt"
	"log"
	"net"
)

const (
	CONN_HOST = "localhost"
	CONN_PORT = "2323"
	CONN_TYPE = "tcp"

	PROMPT          = "# "
	PROMPT_LOGIN    = "login: "
	PROMPT_PASSWORD = "passwd: "

	PROMPT_WELCOME = `
 __          __ ______  _        _____   ____   __  __  ______ 
 \ \        / /|  ____|| |      / ____| / __ \ |  \/  ||  ____|
  \ \  /\  / / | |__   | |     | |     | |  | || \  / || |__   
   \ \/  \/ /  |  __|  | |     | |     | |  | || |\/| ||  __|  
    \  /\  /   | |____ | |____ | |____ | |__| || |  | || |____ 
     \/  \/    |______||______| \_____| \____/ |_|  |_||______|

`
	CMD_EXIT = "exit\r\n"
)

// ls -l command
var command_ls = `
total 0
drwxr-xr-x 41 cocoon staff 1.4K Sep 18  2018 flatbuffers/
drwxr-xr-x  8 cocoon staff  272 Jul 26  2018 go-querystring/
drwxr-xr-x 48 cocoon staff 1.6K Sep 10 14:44 gopacket/
drwxr-xr-x 26 cocoon staff  884 May 14 14:56 uuid/
`

type FakeTelnetServer struct {
	net.Listener

	Port           string
	Prompt         string
	LoginPrompt    string
	PasswordPrompt string
	Welcome        string
	Exit           string
}

func NewFakeTelnetServer(port string, prompt, loginPrompt, passwordPrompt string) (server *FakeTelnetServer, err error) {

	if port == "" {
		port = CONN_PORT
	}
	if prompt == "" {
		prompt = PROMPT
	}
	if loginPrompt == "" {
		loginPrompt = PROMPT_LOGIN
	}
	if passwordPrompt == "" {
		passwordPrompt = PROMPT_PASSWORD
	}
	listener, err := net.Listen(CONN_TYPE, CONN_HOST+":"+port)
	if err != nil {
		fmt.Println("Error listening:", err.Error())
		return server, err
	}
	fmt.Println("Listening on " + CONN_HOST + ":" + port)

	server = &FakeTelnetServer{
		listener, port,
		prompt, loginPrompt, passwordPrompt,
		PROMPT_WELCOME, CMD_EXIT,
	}

	return server, err
}

func (s *FakeTelnetServer) Run(ctx context.Context) {

	// close listener when context done
	go func() {
		<-ctx.Done()
		println("closing listener")
		_ = s.Close()
	}()

	// Close the listener when the application closes.
	defer s.Close()

	for {
		// Listen for an incoming connection.
		conn, err := s.Accept()
		if err != nil {
			fmt.Println("Error accepting: ", err.Error())
			return
		}
		// Handle connections in a new goroutine.
		go handleRequest(conn, s)
	}

}

// Handles incoming requests.
func handleRequest(conn net.Conn, server *FakeTelnetServer) {

	defer conn.Close()
	reader := bufio.NewReader(conn)
	writer := bufio.NewWriter(conn)

	_, err := writer.WriteString(server.Welcome)
	if err != nil {
		log.Println(err.Error())
		return
	}

	state := ""

	for {

		_ = writer.Flush()

		if state == "" {
			login(reader, writer, server)
			state = "LOGGED"
			_, _ = writer.WriteString("# you are logged \r\n")
		}

		_, err := writer.WriteString(server.Prompt)
		if err != nil {
			log.Println(err.Error())
			return
		}
		_ = writer.Flush()

		text, err := reader.ReadString('\n')
		if err != nil {
			log.Println(err.Error())
			return
		}

		if text == server.Exit {
			break
		}
		if text == "ls -l\r\n" {
			_, _ = writer.WriteString(command_ls)
			continue
		}
		// empty line
		if text == "\r\n" {
			continue
		}

		_, _ = writer.WriteString("command not found")

	}

}

func login(r *bufio.Reader, w *bufio.Writer, server *FakeTelnetServer) {

	// display login prompt
	_, err := w.WriteString(server.LoginPrompt)
	if err != nil {
		log.Println(err.Error())
		return
	}
	_ = w.Flush()

	user, err := r.ReadString('\n')
	if err != nil {
		log.Println(err.Error())
		return
	}

	// display password prompt
	_, err = w.WriteString(server.PasswordPrompt)
	if err != nil {
		log.Println(err.Error())
		return
	}
	_ = w.Flush()

	passwd, err := r.ReadString('\n')
	if err != nil {
		log.Println(err.Error())
		return
	}

	_ = user
	_ = passwd
	return
}
