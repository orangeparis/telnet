package telnet_test

import (
	"bitbucket.org/orangeparis/telnet/tests"
	"context"
	"log"
	"strings"
	"testing"
	"time"

	"bitbucket.org/orangeparis/telnet"
	"github.com/nats-io/nats"
)

/*
	test a telnet session with login

	use the fake server /tests/faketelenetserver.go




*/

func TestTelnetSessionWithLogin(t *testing.T) {

	// create a fake telnet server
	ctx, cancel := context.WithCancel(context.Background())

	server, err := tests.NewFakeTelnetServer("2323", "# ", "ogin", "assw")
	if err != nil {
		log.Println(err.Error())
		return
	}
	go server.Run(ctx)
	defer cancel()

	// open a nats connection
	nc, err := nats.Connect("nats://demo.nats.io:4222")
	if err != nil {
		t.Fail()
		return
	}
	defer nc.Close()

	// create a session
	p, err := telnet.NewSessionParams("fake", "localhost", 2323, "root", "pwd", "#", 60, 90)
	if err != nil {
		t.Fail()
		return
	}

	s, err := telnet.NewSession(nc, p)
	defer s.Close()

	err = s.Connect()
	if err != nil {
		log.Println(err.Error())
		t.Fail()
		return
	}

	// wait for login prompt
	log.Println("try login...")
	err = s.Login("# ", "ogin", "assw")
	if err != nil {
		log.Println(err.Error())
		t.Fail()
		return
	}
	log.Println("login OK")

	// flush screen
	_, _ = s.ReadUntil("# ")

	// this time we should be able to read until a ls -l
	err = s.SendLn("ls -l")
	if err != nil {
		log.Println(err.Error())
		t.Fail()
		return
	}
	s.Flush()

	time.Sleep(1 * time.Second)

	data, err := s.ReadUntil("# ")
	if err != nil {
		log.Println(err.Error())
		t.Fail()
		return
	}
	buffer := string(data)
	lines := strings.Split(buffer, "# ")
	print(lines)

	// flush the read buffer
	_ = s.Expect("never catch this")

	// re send a command to the echo server
	myLine := "this is my line"
	err = s.SendLn(myLine)
	if err != nil {
		log.Println(err.Error())
		t.Fail()
		return
	}

}
