package telnet_test

import (
	"log"
	"testing"

	"bitbucket.org/orangeparis/telnet"
	"github.com/nats-io/nats"
)

/*
	test the basic Session connection

	! NEEDS to start an echo telnet server at 5555 see cmd.echo tu run one


*/

func TestBasicTelnetSession(t *testing.T) {

	// open a nats connection
	nc, err := nats.Connect("nats://demo.nats.io:4222")
	if err != nil {
		t.Fail()
		return
	}
	defer nc.Close()

	// create a session
	p, err := telnet.NewSessionParams("fake", "localhost", 5555, "", "", "#", 10, 90)
	if err != nil {
		t.Fail()
		return
	}

	s, err := telnet.NewSession(nc, p)
	defer s.Close()

	err = s.Connect()
	if err != nil {
		log.Println(err.Error())
		t.Fail()
		return
	}

	// send a command to the echo server
	err = s.SendLn("ls -l")
	if err != nil {
		log.Println(err.Error())
		t.Fail()
		return
	}

	// read until a non present
	err = s.Expect("notfound")
	if err == nil {
		log.Println("failed: we should not hace catch this")
		t.Fail()
		return
	}

	// re send a command to the echo server
	err = s.SendLn("ls -l")
	if err != nil {
		log.Println(err.Error())
		t.Fail()
		return
	}

	// this time we should be able to read until a ls -l
	err = s.Expect("ls -l")
	if err != nil {
		log.Println(err.Error())
		t.Fail()
		return
	}

	// flush the read buffer
	s.Expect("never catch this")

	// re send a command to the echo server
	myLine := "this is my line"
	err = s.SendLn(myLine)
	if err != nil {
		log.Println(err.Error())
		t.Fail()
		return
	}

	// this time we should be able to read until a ls -l
	buffer, err := s.ReadUntil(myLine)
	if err != nil {
		log.Println(err.Error())
		t.Fail()
		return
	}
	println(string(buffer))
	if string(buffer) != myLine {
		log.Printf("buffer should be equal to [%s], we got: [%s]", myLine, buffer)
		t.Fail()
		return

	}

}
