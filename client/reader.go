package client

import (
	"encoding/json"
	"github.com/jirenius/go-res"
	"github.com/nats-io/nats"
	"log"
	"time"
)

/*
	a io.Reader implementation to read from a res nats service

	call.telnet.session.<name>.read

*/

type ResReader struct {
	*nats.Conn
	Subject string // eg call.telnet.session.<name>.read
	Timeout time.Duration
}

// Read implements io.Reader Read(buf []byte) (n int, err error)
func (r *ResReader) Read(buf []byte) (n int, err error) {

	// prepare the request
	type Request struct {
		Params interface{} `json:"params"`
	}
	type In struct {
		Timeout int `json:"timeout"`
	}
	type Out struct {
		Buffer []byte `json:"buffer"`
		N      int    `json:"n"` // len of buffer read
		Err    error  `json:"err"`
	}
	type Result struct {
		Result Out       `json:"result"`
		Error  res.Error `json:"error"`
	}

	subject := r.Subject
	in := &In{Timeout: int(r.Timeout)}
	req := &Request{Params: in}
	msg, err := json.Marshal(req)
	if err != nil {
		return n, err
	}
	m, err := r.Request(subject, msg, r.Timeout)
	if err != nil {
		log.Printf("ResReader: request error: %s", err.Error())
		return n, err
	}

	// decode response
	out := Result{}
	err = json.Unmarshal(m.Data, &out)
	if err != nil {
		return n, err
	}

	copy(buf, out.Result.Buffer)
	n = out.Result.N

	return n, err
}
