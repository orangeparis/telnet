package client

import (
	"encoding/json"
	"github.com/nats-io/nats"
	"log"
	"time"
)

/*

	implements a io.Writer for nats res


	 Write(p []byte) (n int, err error)

	call.telnet.session.<name>.write

*/

type ResWriter struct {
	*nats.Conn
	Subject string // eg call.telnet.session.<name>.write
	Timeout time.Duration
}

func (w *ResWriter) Write(p []byte) (n int, err error) {

	// prepare the request
	type Request struct {
		Params interface{} `json:"params"`
	}
	type In struct {
		Timeout int `json:"timeout"`
	}
	type Out struct {
		N   int   `json:"n"` // len of buffer read
		Err error `json:"err"`
	}

	subject := w.Subject
	in := &In{Timeout: int(w.Timeout)}
	req := &Request{Params: in}
	msg, err := json.Marshal(req)
	if err != nil {
		return n, err
	}
	m, err := w.Request(subject, msg, w.Timeout)
	if err != nil {
		log.Printf("ResWriter: request error: %s", err.Error())
		return n, err
	}

	// decode response
	out := Out{}
	err = json.Unmarshal(m.Data, &out)
	if err != nil {
		return n, err
	}

	n = out.N
	err = out.Err

	return n, err
}
