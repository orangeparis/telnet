package client

import (
	"encoding/json"
	"fmt"
	"log"
	"time"

	"bitbucket.org/orangeparis/telnet/service"
	"github.com/nats-io/nats"
)

/*
	a client for nats service telnet
*/

// Client a client for ipwatch nats service
type Client struct {
	*nats.Conn
	Timeout time.Duration
}

type Request struct {
	Params interface{} `json:"params"` // watcher name
}

// NewClient create a nats client to ipwatch service
func NewClient(natsURL string, timeout int) (client *Client, err error) {

	// create a ipv4 watcher controller
	nc, err := nats.Connect(natsURL)
	if err != nil {
		log.Printf("telnet.client: cannot connect to %s\n", natsURL)
		return client, err
	}
	if timeout == 0 {
		timeout = 5
	}

	client = &Client{nc, time.Duration(time.Duration(timeout) * time.Second)}

	return client, err
}

// Open a telnet session  call.telnet.session.open
func (c *Client) Open(name, host string, port int, login, password, prompt string, timeout int, logLevel int) (m *nats.Msg, err error) {

	subject := "call.telnet.session.open"
	in := &service.NewIn{Name: name, Host: host, Port: port, Login: login, Password: password, Timeout: timeout}
	req := &Request{Params: in}
	msg, err := json.Marshal(req)
	if err != nil {
		return m, err
	}
	m, err = c.Request(subject, msg, c.Timeout)
	if err != nil {
		log.Printf("telnet client: request error: %s", err.Error())
		return m, err
	}
	//println(string(m.Data))
	return m, err
}

// List active session  get.telnet.session
func (c *Client) List() (m *nats.Msg, err error) {
	subject := "get.telnet.session"
	m, err = c.Request(subject, []byte("null"), c.Timeout)
	if err != nil {
		log.Printf("error: %s", err)
	}
	return m, err
}

// Close kill session call.telnet.session.$id.close
func (c *Client) Close(id string) (m *nats.Msg, err error) {

	subject := fmt.Sprintf("call.telnet.session.%s.close", id)
	m, err = c.Request(subject, []byte("null"), c.Timeout)
	if err != nil {
		log.Printf("error: %s", err)
	}
	return m, err
}

// GetSession get model of a session : get.telnet.session.$id
func (c *Client) GetSession(id string) (m *nats.Msg, err error) {

	subject := fmt.Sprintf("get.telnet.session.%s", id)
	m, err = c.Request(subject, []byte("null"), c.Timeout)
	if err != nil {
		log.Printf("error: %s", err)
	}
	return m, err
}

// Clear kill all sessions call.telnet.session.clear
func (c *Client) Clear() (m *nats.Msg, err error) {
	subject := "call.telnet.session.clear"
	m, err = c.Request(subject, []byte("null"), c.Timeout)
	if err != nil {
		log.Printf("error: %s", err)
	}
	return m, err
}

// SendLn : Send line to telnet session
func (c *Client) SendLn(session, line string) (m *nats.Msg, err error) {
	subject := fmt.Sprintf("call.telnet.session.%s.sendln", session)
	in := make(map[string]string)
	in["line"] = line
	msgIn, err := json.Marshal(in)
	m, err = c.Request(subject, msgIn, c.Timeout)
	if err != nil {
		log.Printf("error: %s", err)
	}
	return m, err
}

//// Read from a telnet session  msg.data is ( buffer []byte, n int, err error ) {
//func (c *Client) Read(session string, buff []byte) (m *nats.Msg, err error) {
//	subject := fmt.Sprintf("call.telnet.session.%s.read", session)
//	in := make(map[string]string)
//	in["line"] = line
//	msgIn, err := json.Marshal(in)
//	m, err = c.Request(subject, msgIn, c.Timeout)
//	if err != nil {
//		log.Printf("error: %s", err)
//	}
//	return m, err
//}
