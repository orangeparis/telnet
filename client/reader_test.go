package client_test

import (
	"bitbucket.org/orangeparis/telnet/client"
	"bufio"
	"github.com/jirenius/go-res"
	"github.com/nats-io/nats"
	"log"
	"strings"
	"testing"
	"time"
)

/*

	call.service.stream.read


*/

var sampleStream = `a sample stream
of content
to be 
read
via call.service.stream.read

end
# 
`

func mkservice(natsURL string) (s *res.Service) {

	// Create a new RES Service
	s = res.NewService("service")

	// ad the stream resource
	addReaderResource(s)

	// Start the service
	go s.ListenAndServe(natsURL)

	time.Sleep(1 * time.Second)

	return s

}

func addReaderResource(s *res.Service) {

	type In struct {
	}
	type Out struct {
		Buffer []byte `json:"buffer"`
		N      int    `json:"n"` // len of buffer read
		Err    error  `json:"err"`
	}

	// set source
	stream := strings.NewReader(sampleStream)
	bufferSize := 1024

	// handle collection ipwatch.ipv4 resource
	s.Handle("stream",
		res.Access(res.AccessGranted),

		//
		res.Call("read", func(r res.CallRequest) {
			var p = &In{}
			r.ParseParams(p)

			// read the streamer
			buffer := make([]byte, bufferSize)
			n, err := stream.Read(buffer)

			resp := buffer[0:n]

			// prepare response
			result := Out{
				Buffer: resp,
				N:      n,
				Err:    err,
			}
			// send response

			if err != nil {
				r.Error(res.ToError(err))
			} else {
				r.OK(result)
			}
		}),
	)
}

func TestResReader(t *testing.T) {

	natsURL := "nats://demo.nats.io:4222"

	// create a service
	s := mkservice(natsURL)
	defer s.Shutdown()

	// add service.stream resource
	//addReaderResource(s)

	// use the stream service
	nc, err := nats.Connect(natsURL)
	if err != nil {
		t.Fail()
		return
	}
	defer nc.Close()

	// make a ResReader
	reader := &client.ResReader{
		Conn:    nc,
		Subject: "call.service.stream.read",
		Timeout: 1 * time.Second,
	}

	received := make([]byte, 1024)
	n, err := reader.Read(received)

	if err != nil {
		log.Printf("error: %s\n", err.Error())
	} else {
		log.Printf("receive %d bytes\n", n)
		log.Printf("buffer:%s\n", string(received[0:n]))
	}

}

func TestBufferedResReader(t *testing.T) {

	natsURL := "nats://demo.nats.io:4222"

	// create a service
	s := mkservice(natsURL)
	defer s.Shutdown()

	// use the stream service
	nc, err := nats.Connect(natsURL)
	if err != nil {
		t.Fail()
		return
	}
	defer nc.Close()

	// make a ResReader
	reader := &client.ResReader{
		Conn:    nc,
		Subject: "call.service.stream.read",
		Timeout: 1 * time.Second,
	}

	bufreader := bufio.NewReader(reader)

	//received := make([]byte, 1024)
	//n, err := bufreader.Read(received)

	for {

		l, err := bufreader.ReadString('\n')
		if err != nil {
			log.Printf("error: %s\n", err.Error())
			break
		} else {
			log.Printf("line: %s\n", string(l))
			l, err := bufreader.Peek(2)
			if err != nil {
				log.Printf("no more data\n")
				break
			} else {
				if string(l) == "# " {
					log.Printf("final prompt\n")
					break
				}
			}
		}

	}
}
